import Vue from 'vue'

export default {
  namespaced: true,
  state () {
    return {
      token: '',
      current_user_id: '',
      current_user_avataro_bildo: '',
      current_user_nomo: '',
      errors: []
    }
  },
  getters: {
    token (state) {
      return state.token
    },
    current_user_id (state) {
      return state.current_user_id
    },
    current_user_avataro_bildo (state) {
      return state.current_user_avataro_bildo
    },
    current_user_nomo (state) {
      return state.current_user_nomo
    },
    errors (state) {
      return state.errors
    }
  },
  mutations: {
    setUserData (state, data) {
      state.current_user_id = data.id
      state.current_user_avataro_bildo = data.avataro_bildo
      state.current_user_nomo = data.unua_nomo
    },
    setToken (state, token) {
      state.token = token
    },
    errors (state, error) {
      state.errors.push(error)
    }
  },
  actions: {
    login (store, payLoad) {
      Vue.http.post('https://tehnokom.su/api/v1/get-token/', {
        username: payLoad.username,
        password: payLoad.password
      })
        .then(response => store.commit('setToken', response.data.token))
        .catch(e => store.commit('errors', e))
    },
    logout (store) {
      store.commit('setToken', '')
    },
    getUserData (store, token) {
      Vue.http.get('https://tehnokom.su/api/v1/uzantoj/mi/',
        {
          headers: {
            'Authorization': 'Token ' + token
          }
        })
        .then(response => store.commit('setUserData', response.data))
        .catch(e => store.commit('errors', e))
    }
  }
}
